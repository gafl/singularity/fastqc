# fastqc Singularity container
### Bionformatics package fastqc<br>
A quality control tool for high throughput sequence data.<br>
fastqc Version: 0.11.8<br>
[http://www.bioinformatics.babraham.ac.uk/projects/fastqc/]

Singularity container based on the recipe: Singularity.fastqc_v0.11.8

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build fastqc_v0.11.8.sif Singularity.fastqc_v0.11.8`

### Get image help
`singularity run-help ./fastqc_v0.11.8.sif`

#### Default runscript: STAR
#### Usage:
  `fastqc_v0.11.8.sif --help`<br>
    or:<br>
  `singularity exec fastqc_v0.11.8.sif fastqc --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull fastqc_v0.11.8.sif oras://registry.forgemia.inra.fr/gafl/singularity/fastqc/fastqc:latest`


